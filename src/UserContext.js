import React from 'react';

// Creates UserContext object
/*
	createContext method allows us to store different data inside an object, in this case called UserContext.
	This set of information can be shared to other components within the app.

	a different approach in passing information between components without the use of props and pass it from parent to child component.	
*/
const UserContext = React.createContext();

/*
	Provider component allows other components to consume/use the context object and supply the necessary information needed to the context object 
*/

// export (without default) needs destructuring before being able to be imported by other components
export const UserProvider = UserContext.Provider;

export default UserContext;