import { Fragment, useEffect, useState } from 'react';

import CourseCard from '../components/CourseCard.js'
// import coursesData from '../data/coursesData.js'

/*
	Miniactivity 
		move the courseCard component from Home.js to Courses.js

		replace the h1 element in the return statement with the "sample course" card from the Home Page
*/

/*
	the course in our CourseCard component can be sent as a prop
		prop - short for property, since the components are considered as objects in ReactJS
*/

/*
	the map method loops through the individual course objects and returns a component for each course 

	multiple components created map method must have a unique key that will help reactJS identify which component/element have been changed/added/moved
*/

export default function Courses() {
	// console.log(coursesData);

	const [ courses, setCourses ] = useState([]);

	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=> {
			console.log(data);

			setCourses(data.map(course=>{
				return(
					<CourseCard key={course.id} courseProp={course} />	
				);
			}));
		})
	},[])
/*
	const courses = coursesData.map(course => {
		return(
			<CourseCard key = {course.id} courseProp = {course}/>
		)
	})*/

		return (
			<Fragment>
				{courses}
			</Fragment>
		)
}