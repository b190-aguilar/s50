import {Button, Form} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register(){
/*
	Miniactivity
		1. create email, password1, and password2 variables using useState and set their initial values to empty strings
		2. create isActive variable using useState and set the initial value to false
		3. refactor the button and add a ternary operator stating that if the isActive is true, the button will be clickable, disabled if false
*/

	// state hooks to store the values 
	const [ firstName, setFirstName ] = useState("");
	const [ lastName, setLastName ] = useState("");
	const [ mobileNo, setMobileNo] = useState("");
	const [ email, setEmail ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");
	const [ isActive, setIsActive ] = useState(false);

	// UserContext
	const { user } = useContext(UserContext);
	console.log(user)

	// to check if we have successfully implemented the 2-way binding
	/*
		whenever a state of a component changes, the component renders the whole component executing the code found in the component itself

		e.target.value - allows us to gain access to the input field's current value to be used when submitting form data
	*/
	console.log(`email: ${email}`);
	console.log(`password1: ${password1}`);
	console.log(`password2: ${password2}`);
	console.log(`firstName: ${firstName}`);
	console.log(`lastName: ${lastName}`);
	console.log(`mobileNo: ${mobileNo}`);
	// console.log(mobileNo.length	);


	// state to determine whether the submit button is enabled or not
	useEffect(() => {
		if( (email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "" && mobileNo !== "") && (password1 === password2) && (mobileNo.length === 11) ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
		
	}, [email,password1,password2, firstName, lastName, mobileNo]);


	//
	function checkEmailExists(e){	
		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
				title: "Registration Failed",
				icon: "error",
				text:"Email already used."
				});
			}
			else{
				registerUser(e);
			}
		});

		//clear input fields
		/*
			setter functions can also bring back the original states of the getter variables if the dev wants to
		*/
		setEmail("");
		setPassword1("");
		setPassword2("");

	}

	function registerUser(e){
		e.preventDefault();

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers:{
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				firstName: firstName,
				lastName: lastName,
				mobileNo: mobileNo, 
				password: password1,
			})
		})
		.then(res => res.json())
		.then(user => {
			console.log(user);
			Swal.fire({
			title: "Registration Successful!",
			icon:"success",
			text: "Thank you for Registering!"
			})
		})
	}	
		

		return (
		(user.id !== null) ?
		<Navigate to='/courses' />
		:
	  <Form onSubmit={(e) => checkEmailExists(e)}>
	  	<Form.Group controlId="firstName">
	      <Form.Label>First Name</Form.Label>
	      <Form.Control 
	      				type="string" 
	      				// placeholder="First Name" 
	      				value={firstName}
	      				onChange={e => setFirstName(e.target.value)}
	      				required 
			/>
	    </Form.Group>

	    <Form.Group controlId="lastName">
	      <Form.Label>Last Name</Form.Label>
	      <Form.Control 
	      				type="string" 
	      				// placeholder="Last Name" 
	      				value={lastName}
	      				onChange={e => setLastName(e.target.value)}
	      				required 
			/>
	    </Form.Group>

	    <Form.Group controlId="mobileNo">
	      <Form.Label>Mobile No</Form.Label>
	      <Form.Control 
	      				type="string" 
	      				placeholder="09XXXXXXXXX" 
	      				value={mobileNo}
	      				onChange={e => setMobileNo(e.target.value)}
	      				required 
			/>
	    </Form.Group>

	    <Form.Group controlId="userEmail">
	      <Form.Label>Email address</Form.Label>
	      <Form.Control 
	      				type="email" 
	      				placeholder="Enter email" 
	      				value={email}
	      				onChange={e => setEmail(e.target.value)}
	      				required 
			/>
	      <Form.Text className="text-muted">
	        We'll never share your email with anyone else.
	      </Form.Text>
	    </Form.Group>

	    <Form.Group controlId="password1">
	      <Form.Label>Password</Form.Label>
	      <Form.Control
	      				type="password" 
	      				placeholder="Password" 
	      				value={password1}
	      				onChange={e => setPassword1(e.target.value)}
	      				required
			/>
	    </Form.Group>

	    <Form.Group controlId="password2">
	      <Form.Label>Verify Password</Form.Label>
	      <Form.Control 
	      				type="password" 
	      				placeholder="Password" 
	      				value={password2}
	      				onChange={e => setPassword2(e.target.value)}
	      				required
			/>
	    </Form.Group>
	    {isActive ?
	    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	    :
	    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
		}
	  </Form>
	);
}
