// import { Link } from 'react-router-dom';
// import { Row } from 'react-bootstrap';

import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: '404 Not Found',
		content: 'The page you are looking for cannot be found.',
		destination: '/',
		label: 'Back Home'
	}

	return(
		<Banner data={data}/>
		)

/*	return(
		<Row>
			<h1>Page Not Found</h1>
			<p>Go back to <Link to='/'>homepage</Link></p>
		</Row>

	)*/
}
