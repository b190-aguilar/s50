import { Navigate } from 'react-router-dom';
import { useEffect, useContext } from 'react';
import UserContext from '../UserContext';

/*
	MINIACTIVITY
		- in App.js, add the unsetUser to be used by other components in the web application. 
		- import the following:
			- useContext
			- UserContext
			- useEffect
		- create a unsetUser variable that has the value coming from the UserContext
		- use the useEffect and setUser to set the user value into null (this should be in the form of an object)
		- delete the localStorage.clear() method 

*/


export default function Logout() {
	// consumes the UserContext object and destructure it to access the unsetUser and setUser states
	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	/*
		placing the 'setUser' setter function inside of a useEffect is necessary because of the updates in React HS that a state of another component cannot be updated while trying to render a different component

		by adding useEffect, this will render Logout page first before triggering the useEffect which changes the state of the User.
	*/

	// sets the user state back to its original state.
	useEffect(() => {
		setUser({id:null})
	});

	return(
		<Navigate to='/login' />
	)
}
