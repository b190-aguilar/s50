// react permits the use of objects literals for importing multiple components from the same dependency
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	const { title, content, destination, label } = data;

	return(
		<Row>
			<Col classname ="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link to ={destination}>{label}</Link>
			</Col>
		</Row>
		)
}